package com.rik.mockproject.controller.admin;

import com.rik.mockproject.excel.ProductDataExcelExport;
import com.rik.mockproject.exception.ProductNotFoundException;
import com.rik.mockproject.model.Product;
import com.rik.mockproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
@Controller
@RequestMapping("/admin/products")
public class AdminProductController {
    @Autowired
    ProductService productService;


    @GetMapping("/list_all")
    public String getAllProduct(@RequestParam(value = "message", required = false) String message, Model model) {
        List<Product> productList = productService.fillAllProduct();
        model.addAttribute("productsList", productList);
        model.addAttribute("message", message);
        return "admin/product";
    }
    @GetMapping("/delete")
    public String deleteProduct(@RequestParam Integer id, RedirectAttributes attributes) {

        try {
            productService.deleteProduct(id);
            attributes.addAttribute("message", "Product with Id : '"+id+"' is removed successfully!");
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
            attributes.addAttribute("message", e.getMessage());
        }
        return "redirect:list_all";
    }

    @GetMapping("/edit_product")
    public String getEditPage(Model model, RedirectAttributes attributes, @RequestParam Integer id){
        String page = null;
        try {
            Product product = productService.getProductById(id);
            model.addAttribute("product", product);
            page="admin/editProduct";
        } catch (ProductNotFoundException e) {
            e.printStackTrace();
            attributes.addAttribute("message", e.getMessage());
            page="redirect:list_all";
        }
        return page;
    }

    @PostMapping("/update_product")
    public String updateProduct(@ModelAttribute Product product, RedirectAttributes attributes ){
        productService.addProduct(product);
        Integer id = product.getId();
        attributes.addAttribute("message", "Product with id: '"+id+"' is updated successfully !");
        return "redirect:list_all";
    }

    @PostMapping("/save_product")
    public String addProduct(@ModelAttribute Product product, Model model) {
        productService.addProduct(product);
        Integer id = productService.addProduct(product).getId();
        String message = "Record with id: '"+id+"' is saved successfully !";
        model.addAttribute("message",message);
        return "admin/addProduct";
    }

    @GetMapping("/add_product")
    public String showRegistration() {
        return "admin/addProduct";
    }


    @GetMapping("/excelExport")
    public ModelAndView exportToExcel() {
        ModelAndView mav = new ModelAndView();
        mav.setView(new ProductDataExcelExport());
        //read data from DB
        List<Product> list= productService.fillAllProduct();
        //send to excelImpl class
        mav.addObject("list", list);
        return mav;
    }
}
