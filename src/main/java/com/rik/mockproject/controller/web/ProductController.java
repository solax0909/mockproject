package com.rik.mockproject.controller.web;

import com.rik.mockproject.dto.ProductDTO;
import com.rik.mockproject.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/products")
public class ProductController {
    @Autowired
    ProductService productService;


    @GetMapping("/list_by_category")
    public ResponseEntity<List<ProductDTO>> getAllProductByCategory(@RequestParam Integer categoryId) {
        List<ProductDTO> lisByCategoryId = productService.loadProductListByCategoryId(categoryId);
        return new ResponseEntity<>(lisByCategoryId, HttpStatus.OK);
    }

    @GetMapping("/list_by_price")
    public ResponseEntity<List<ProductDTO>> getAllProductByPrice() {
        List<ProductDTO> lisByPrice = productService.listProductByPrice();
        return new ResponseEntity<List<ProductDTO>>(lisByPrice, HttpStatus.OK);
    }
    @GetMapping(value = "/get_details/{id}")
    public ResponseEntity<ProductDTO> getProductDetails(@PathVariable Integer id) {

        ProductDTO productDTO= productService.loadProductDetails(id);
        return new ResponseEntity<>(productDTO, HttpStatus.OK);
    }
}
