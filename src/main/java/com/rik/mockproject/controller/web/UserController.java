package com.rik.mockproject.controller.web;

import com.rik.mockproject.dto.LoginUser;
import com.rik.mockproject.dto.UserDTO;
import com.rik.mockproject.model.User;
import com.rik.mockproject.service.impl.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/")
public class UserController {

    @Autowired
    UserServiceImpl userService;

    //tạo đường dẫn mặc định cho home
    @GetMapping("/home")
    public String viewHomePage() {
        return "index";
    }

    @PostMapping("/saveUser")
    public String saveUser(@ModelAttribute("user") UserDTO userDTO, Model model) {
        userService.saveUser(userDTO);
        UserDTO user = new UserDTO();
        model.addAttribute("user", user);
        LoginUser loginUser = new LoginUser();
        model.addAttribute("userLogin", loginUser);
        return "account";
    }

    @GetMapping("/login")
    public String login(Model model) {
        UserDTO userDTO = new UserDTO();
        model.addAttribute("user", userDTO);
        LoginUser loginUser = new LoginUser();
        model.addAttribute("userLogin", loginUser);
        return "account";
    }

    @PostMapping("/login")
    public String loginWeb(@ModelAttribute("userLogin") LoginUser loginUser, HttpServletRequest request) {
        User user = userService.findByUsernameAndPassword(loginUser.getUsername(), loginUser.getPassword());
        HttpSession httpSession = request.getSession();
        if (user != null) {
            httpSession.setAttribute("login", user);
            return "redirect:home";
        }
        return "account";
    }


}
