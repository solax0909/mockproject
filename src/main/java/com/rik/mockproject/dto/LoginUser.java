package com.rik.mockproject.dto;

import lombok.Data;

@Data
public class LoginUser {
    private String username;
    private String password;
}
