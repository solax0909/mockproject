package com.rik.mockproject.dto;

import lombok.Data;

@Data
public class StatisticDTO {
    private Integer year;
    private Integer month;
    private Long sum;
    private Integer count;

    public StatisticDTO(Integer year, Integer month, Long sum) {
        this.year = year;
        this.month = month;
        this.sum = sum;
    }

    public StatisticDTO(Integer month, Long sum) {
        this.month = month;
        this.sum = sum;
    }

    public StatisticDTO(Integer month, Integer count) {
        this.month = month;
        this.count = count;
    }
}
