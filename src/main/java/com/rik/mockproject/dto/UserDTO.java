package com.rik.mockproject.dto;

import lombok.Data;

//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
//@EqualsAndHashCode
@Data
public class UserDTO {
    private String firstname;
    private String lastname;
    private String username;
    private String password;
    private String phone;
    private String email;
}