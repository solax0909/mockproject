package com.rik.mockproject.excel;

import com.rik.mockproject.model.Product;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

public class ProductDataExcelExport extends AbstractXlsView {
    @Override
    protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request, HttpServletResponse response) throws Exception {
        response.addHeader("Content-Disposition", "attachment;fileName=ProductData.xlsx");

        // read data provided by controller
        @SuppressWarnings("unchecked")
        List<Product> list = (List<Product>) model.get("list");

        // create one sheet
        Sheet sheet = workbook.createSheet("Product");

        // create row0 as a header
        Row row0 = sheet.createRow(0);
        row0.createCell(0).setCellValue("ID");
        row0.createCell(1).setCellValue("NAME");
        row0.createCell(2).setCellValue("DESCRIPTIONS");
        row0.createCell(3).setCellValue("PRICE");
        row0.createCell(4).setCellValue("QUANTITY");
        row0.createCell(5).setCellValue("CATEGORY_ID");
        row0.createCell(6).setCellValue("BRAND_ID");
        row0.createCell(7).setCellValue("IMAGE_ID");
        row0.createCell(8).setCellValue("STATUS");

        // create row1 onwards from List<T>
        int rowNum = 1;
        for (Product spec : list) {
            Row row = sheet.createRow(rowNum++);
            row.createCell(0).setCellValue(spec.getId());
            row.createCell(1).setCellValue(spec.getName());
            row.createCell(2).setCellValue(spec. getDescriptions());
            row.createCell(3).setCellValue(spec. getPrice());
            row.createCell(4).setCellValue(spec. getQuantity());
            row.createCell(5).setCellValue(spec. getCategoryId());
            row.createCell(6).setCellValue(spec.getBrandId());
            row.createCell(7).setCellValue(spec. getImageId());
            row.createCell(8).setCellValue(spec. getStatus());


        }
    }
}
