package com.rik.mockproject.mapper;

import com.rik.mockproject.dto.ProductDTO;
import com.rik.mockproject.model.Product;
import com.rik.mockproject.vo.ProductDetailVO;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface ProductMapper {

    //  @Mappings({})
    Product toEntity(ProductDTO product);

    //  @Mappings({})
    ProductDTO toModel(Product entity);

    ProductDTO toModelV2(ProductDetailVO vo);
}

