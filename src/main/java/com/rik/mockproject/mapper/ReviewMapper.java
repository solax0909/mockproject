package com.rik.mockproject.mapper;
import com.rik.mockproject.dto.ReviewDTO;
import com.rik.mockproject.model.Review;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Mapper(componentModel = "spring")
@Component
public interface ReviewMapper {
    //  @Mappings({})
    Review toEntity(ReviewDTO product);

    //  @Mappings({})
    ReviewDTO toModel(Review entity);
}
