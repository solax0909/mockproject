package com.rik.mockproject.model;

import com.rik.mockproject.dto.StatisticDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter

@NamedNativeQuery(name ="statistic_of_month", query = "select month(o.create_at) as month, sum(od.price) as sum from order_details od\n" +
        "inner join orders o on od.order_id = o.id\n" +
        "where year(o.create_at) = year(now())\n" +
        "group by month(o.create_at);", resultSetMapping = "statisticOfMonth")

@SqlResultSetMapping(
        name = "statisticOfMonth",
        classes = @ConstructorResult(
                targetClass = StatisticDTO.class,
                columns = {
                        @ColumnResult(name = "month", type = Integer.class),
                        @ColumnResult(name = "sum", type = Long.class)
                }
        )
)

@NamedNativeQuery(name = "statistic_of_year", query = "select year(o.create_at) as year, sum(od.price) as sum from order_details od\n" +
        "inner join orders o on od.order_id = o.id\n" +
        "where year(o.create_at) = year(now())\n" +
        "group by year(o.create_at);", resultSetMapping = "statisticOfYear")

@SqlResultSetMapping(
        name = "statisticOfYear",
        classes = @ConstructorResult(
                targetClass = StatisticDTO.class,
                columns = {
                        @ColumnResult(name = "year", type = Integer.class),
                        @ColumnResult(name = "sum", type = Long.class)
                }
        )
)

@NamedNativeQuery(name = "statistic_by_order", query = "select month(o.create_at) as month, count(distinct o.id) as count from order_details od\n" +
        "inner join orders o on od.order_id = o.id\n" +
        "where year(o.create_at) = year(now())\n" +
        "group by month(o.create_at);", resultSetMapping = "statisticByOrder")

@SqlResultSetMapping(
        name = "statisticByOrder",
        classes = @ConstructorResult(
                targetClass = StatisticDTO.class,
                columns = {
                        @ColumnResult(name = "month", type = Integer.class),
                        @ColumnResult(name = "count", type = Integer.class)
                }
        )
)

public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private Date create_at;
    private Double total;
    private Integer user_id;
    private String receiver;
    private String address;
    private String phone;
    private Integer payment_id;
    private Byte status;
}
