package com.rik.mockproject.repository;

import com.rik.mockproject.dto.ProductDTO;
import com.rik.mockproject.model.Product;
import com.rik.mockproject.vo.ProductDetailVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ProductRepository extends JpaRepository<Product, Integer> {
    @Modifying
    @Query(value = "update products set status = 0 where id = :id", nativeQuery = true)
    void deleteById(@Param("id") Integer id);


    @Modifying
    @Query(value = "select pr.name, pr.price, im.path_image from " +
            " products pr join images im on pr.image_id = im.id " +
            "where pr.category_id = :categoryId and pr.status=1 order by pr.name", nativeQuery = true)
    List<Product> findAllByCategoryIdOrderByName(@Param("categoryId") Integer categoryId);

    @Modifying
    @Query(value = "SELECT \n" +
            "    pr.id,\n" +
            "    pr.`name`,\n" +
            "    pr.price,\n" +
            "    pr.descriptions,\n" +
            "    br.`name` AS brandName,\n" +
            "    im.image as pathImage\n" +
            "FROM\n" +
            "    products pr\n" +
            "        JOIN\n" +
            "    images im ON pr.image_id = im.id\n" +
            "        JOIN\n" +
            "    brands br ON pr.brand_id = br.id\n" +
            "WHERE\n" +
            "    pr.status = 1\n" +
            "ORDER BY pr.price ", nativeQuery = true)
    List<ProductDetailVO> listProductByPrice();

    @Modifying
    @Query(value = "select * from products where status = 1", nativeQuery = true)
    List<Product> listAllProduct();

    @Query(
            value = "SELECT \n" +
                    "    pr.id,\n" +
                    "    pr.`name`,\n" +
                    "    pr.price,\n" +
                    "    pr.descriptions,\n" +
                    "    br.`name` AS brandName,\n" +
                    "    im.image AS pathImage\n" +
                    "FROM\n" +
                    "    products pr\n" +
                    "        JOIN\n" +
                    "    images im ON pr.image_id = im.id\n" +
                    "        JOIN\n" +
                    "    brands br ON pr.brand_id = br.id\n" +
                    "WHERE\n" +
                    "    pr.id = :id and pr.status = 1",
            nativeQuery = true)
    ProductDetailVO getProductDetailsById(@Param("id") Integer id);

    @Query(value = "SELECT \n" +
            "    pr.name, pr.price, pr.descriptions, im.image as pathImage, br.name as brandName\n" +
            "FROM\n" +
            "    products pr\n" +
            "        JOIN\n" +
            "    images im ON pr.image_id = im.id\n" +
            "        JOIN\n" +
            "    brands br ON pr.brand_id = br.id\n" +
            "WHERE\n" +
            "    pr.category_id = 1 AND pr.status = 1", nativeQuery = true)
    List<ProductDetailVO> getAllProductByCategoryId(@Param("id") Integer id);
}
