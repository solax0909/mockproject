package com.rik.mockproject.repository;

import com.rik.mockproject.dto.ReviewDTO;
import com.rik.mockproject.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public interface ReviewRepository extends JpaRepository<Review, Integer> {


    List<Review> findAllByProductId(Integer productId);


    @Query(value = "select * from reviews join products on reviews.product_id = products.id where  reviews.id = :id", nativeQuery = true)
    Review updateReviewById( Integer id);

}
