package com.rik.mockproject.repository;

import com.rik.mockproject.dto.StatisticDTO;
import com.rik.mockproject.model.Order;
import jdk.jfr.Enabled;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

@Repository
public interface StatisticRepository extends JpaRepository<Order, Integer> {

    @Query(name = "statistic_of_month", nativeQuery = true)
    public StatisticDTO statisticOfMonth();

    @Query(name = "statistic_of_year", nativeQuery = true)
    public StatisticDTO statisticOfYear();

    @Query(name = "statistic_by_order", nativeQuery = true)
    public StatisticDTO statisticByOrder();
}
