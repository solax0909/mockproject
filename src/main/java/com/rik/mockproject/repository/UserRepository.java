package com.rik.mockproject.repository;

import com.rik.mockproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    Optional<User> findByUsernameOrPhoneOrEmail(String username, String phone, String email);

    Optional<User> findByUsernameAndPassword(String username, String password);
}
