package com.rik.mockproject.service;

import com.rik.mockproject.dto.ProductDTO;
import com.rik.mockproject.model.Product;
import com.rik.mockproject.vo.ProductDetailVO;

import java.util.List;

public interface ProductService {
        List<Product> fillAllProduct();
        List<Product> findProductByCategoryId(Integer categoryId);
        List<ProductDTO> listProductByPrice();

        Product addProduct(Product product);
        void deleteProduct(Integer id);
        Product getProductById(Integer id);

        List<ProductDTO> loadAllProducts();

        void insertNewProduct(ProductDTO product);

        ProductDTO loadProductDetails(Integer id);

        List<ProductDTO> loadProductListByCategoryId(Integer id);
    }

