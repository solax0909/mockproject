package com.rik.mockproject.service;

import com.rik.mockproject.dto.ReviewDTO;
import com.rik.mockproject.model.Review;

import java.util.List;

public interface ReviewService {
    List<ReviewDTO> listReviewByProductId(Integer productId);

    Review getRiviewById(Integer reviewId);

    void deleteById(Integer reviewId);

    List<Review> findAllReview();
    void addReview(Review review);
}
