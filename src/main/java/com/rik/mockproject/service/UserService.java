package com.rik.mockproject.service;

import com.rik.mockproject.dto.UserDTO;
import com.rik.mockproject.model.User;

import java.util.List;

public interface UserService {
    List<User> getAllUser();
    User getUserById(Integer id);
    boolean saveUser(UserDTO userDTO);
    User findByUsernameAndPassword(String username, String password);

    void deleteUserAdmin(Integer id);
    void newUserAdmin(User user);
    void editUserAdmin(User user);

}