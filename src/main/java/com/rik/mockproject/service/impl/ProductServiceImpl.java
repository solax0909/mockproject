package com.rik.mockproject.service.impl;

import com.rik.mockproject.dto.ProductDTO;
import com.rik.mockproject.exception.ProductNotFoundException;
import com.rik.mockproject.mapper.ProductMapper;
import com.rik.mockproject.model.Product;
import com.rik.mockproject.repository.ProductRepository;
import com.rik.mockproject.service.ProductService;
import com.rik.mockproject.vo.ProductDetailVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    ProductMapper mapper;
    @Autowired
    ProductRepository productRepository;

    @Override
    public List<Product> fillAllProduct() {
        return productRepository.listAllProduct();
    }


    @Override
    public List<Product> findProductByCategoryId(Integer categoryId) {
        return productRepository.findAllByCategoryIdOrderByName(categoryId);
    }

    @Override
    public List<ProductDTO> listProductByPrice() {
        return productRepository.listProductByPrice()
                .stream()
                .map(mapper::toModelV2)
                .collect(Collectors.toList());
    }

    @Override
    public Product addProduct(Product product) {
        return productRepository.save(product);
    }

    @Override
    public void deleteProduct(Integer id) {
        productRepository.deleteById(id);
    }

    @Override
    public Product getProductById(Integer id) {
        Optional<Product> opt = productRepository.findById(id);
        if (opt.isPresent()) {
            return opt.get();
        } else {
            throw new ProductNotFoundException("Product with id: " + id + "Not found");
        }
    }
    @Override
    public List<ProductDTO> loadAllProducts() {
        return productRepository.findAll()
                .stream()
                .map(mapper::toModel)
                .collect(Collectors.toList());
    }
    @Override
    public void insertNewProduct(ProductDTO product) {
        this.productRepository.save(this.mapper.toEntity(product));
    }

    @Override
    public ProductDTO loadProductDetails(Integer id) {
        return mapper.toModelV2(productRepository.getProductDetailsById(id));
    }

    @Override
    public List<ProductDTO> loadProductListByCategoryId(Integer id) {
        return productRepository.getAllProductByCategoryId(id)
                .stream()
                .map(mapper::toModelV2)
                .collect(Collectors.toList());
    }

}