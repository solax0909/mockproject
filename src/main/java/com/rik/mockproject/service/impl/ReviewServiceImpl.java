package com.rik.mockproject.service.impl;

import com.rik.mockproject.dto.ReviewDTO;
import com.rik.mockproject.mapper.ReviewMapper;
import com.rik.mockproject.model.Review;
import com.rik.mockproject.repository.ReviewRepository;
import com.rik.mockproject.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ReviewServiceImpl implements ReviewService {
    @Autowired
    ReviewRepository reviewRepository;

    @Autowired
    ReviewMapper reviewMapper;

    @Override
    public List<ReviewDTO> listReviewByProductId(Integer productId) {
        return reviewRepository
                .findAllByProductId(productId)
                .stream()
                .map(reviewMapper::toModel)
                .collect(Collectors.toList());
    }

    @Override
    public Review getRiviewById(Integer reviewId) {
        return reviewRepository.updateReviewById(reviewId);
    }

    @Override
    public void deleteById(Integer reviewId) {
        reviewRepository.deleteById(reviewId);
    }

    @Override
    public List<Review> findAllReview() {
        return reviewRepository.findAll();
    }

    @Override
    public void addReview(Review review) {
        reviewRepository.save(review);
    }

}
