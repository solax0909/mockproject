package com.rik.mockproject.service.impl;

import com.rik.mockproject.dto.UserDTO;
import com.rik.mockproject.model.User;
import com.rik.mockproject.repository.UserRepository;
import com.rik.mockproject.service.UserService;
import com.rik.mockproject.utils.mapping.MappingUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @Override
    public void deleteUserAdmin(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public User getUserById(Integer id) {
        return userRepository.findById(id).get();
    }

    @Override
    public boolean saveUser(UserDTO userDTO) {
        User user = MappingUtil.mapToUser(userDTO);
        if (userRepository.findByUsernameOrPhoneOrEmail(user.getUsername(), user.getPhone(), user.getEmail()).isEmpty()){
            user.setRole_id(1);
            userRepository.save(user);
            return true;
        }
        return false;
    }

    @Override
    public User findByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password).get();
    }

    @Override
    public void newUserAdmin(User user) {
        if(userRepository.findByUsernameOrPhoneOrEmail(user.getUsername(), user.getPhone(), user.getEmail()).isEmpty()){
            userRepository.save(user);
        }
    }

    @Override
    public void editUserAdmin(User user) {
        userRepository.save(user);
    }

}
