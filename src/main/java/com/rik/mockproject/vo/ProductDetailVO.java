package com.rik.mockproject.vo;

public interface ProductDetailVO {
    Integer getId();
    String getName();
    Double getPrice();
    String getBrandName();
    String getDescriptions();
    String getPathImage();
}

